# Begleitbeispiele zu RESTlos sicher

Herzlich willkommen zum Git-Repository der Begleitbeispiele
zum JavaMagazin-Artikel
**RESTlos glückliches REST-Testen**. Dieses Repository
enthält alle zu den Artikel gehörenden Codebeispiele. Die Beispiele
ermöglichen einen schnellen und direkten Einstieg 
in die Nutzung von [REST-assured](http://code.google.com/p/rest-assured/)
im Zusammenspiel mit [JBoss Arquillian](http://www.arquillian.org).

# Ausführung der Beispiele

Die Beispiele sind in der Form von Unittests geschrieben und 
können entweder in jeder beliebigen IDE einzeln
oder via Apache Maven ausgeführt werden.

Als Unittest-Framework kommt [TestNG](http://www.testng.org) zum
Einsatz. Die für die Ausführung der Testfälle benötigte
REST-Schnittstelle ist mit Hilfe von 
[Arquillian](http://www.arquillian.org) und 
[Glassfish](http://www.glassfish.org) realisiert worden.

Da als Build-System für die Beispiel Apache Maven
gewählt wurde, reicht ein initiales Klonen des Repositorys
gefolgt von einem anschließenden Maven-Build vollkommen aus,
um alle notwendigen Artefakte herunterzuladen und die 
Beispiele auszuführen.

    $ git clone https://bitbucket.org/obfischer/restassureddemo.git
    $ cd restassureddemo
    $ mvn

## Umfang der Maven-Dependencys

Die für die Ausführung der Beispiele notwendigen Maven-Dependencys
haben ein Umfang von ungefähr 200 MiB. Daher kann die erstmalige
Ausführung der Beispiel in Abhängigkeit von der Netzanbindung
länger dauern. Genug Zeit für einen längeren Gang zur Kaffeemaschine.

Ich wünsche Ihnen viel Freude mit REST-assured und ein
_bugfreies RESTen_.

Oliver B. Fischer // [http://www.swe-blog.net](http://www.swe-blog.net)
