package net.sweblog.jm.restlos.webappb;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import net.sweblog.jm.restlos.webappb.model.Message;
import net.sweblog.jm.restlos.webappb.rest.MarshallerProvider;
import net.sweblog.jm.restlos.webappb.util.MapperBuilder;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.URL;

import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.config.ObjectMapperConfig.objectMapperConfig;
import static java.lang.String.format;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;

@Test(priority = 10)
@RunAsClient
public class IsAliveIT
    extends Arquillian
{
    @ArquillianResource
    private URL baseURL;

    @BeforeClass
    public void configureObjectMapperForRestAssured()
    {
        ObjectMapperConfig objectMapperConfig = objectMapperConfig().jackson2ObjectMapperFactory(
            new Jackson2ObjectMapperFactory() {
                @Override
                public com.fasterxml.jackson.databind.ObjectMapper create(Class c, String s) {
                    return new MapperBuilder().build();
                }
            });

        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(objectMapperConfig);
    }

    @BeforeMethod
    public void configureRestAssured()
    {
        int port = baseURL.getPort();
        String host = baseURL.getHost();
        String basePath = baseURL.getPath();
        String protocol = baseURL.getProtocol();

        RestAssured.baseURI = format("%s://%s", protocol, host);
        RestAssured.port = port;
        RestAssured.basePath = basePath;
    }

    @Deployment
    public static WebArchive buildDeployment()
    {
        WebArchive war = ShrinkWrap.create(WebArchive.class);

        war.addPackage(WebAppB.class.getPackage());
        war.addPackage(Message.class.getPackage());
        war.addPackage(MarshallerProvider.class.getPackage());

        war.addAsWebInfResource("WEB-INF/glassfish-web.xml")
           .addAsWebInfResource("WEB-INF/web.xml");

        return war;
    }

    public void getIsAliveIsPossible()
    {
        expect().statusCode(200)
                .body(is(equalTo("WebApplication WebApp B is alive!")))
        .when().get(WebAppBConstansts.PATH_TO_IS_ALIVE);
    }
}
