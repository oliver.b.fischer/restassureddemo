package net.sweblog.jm.restlos.webappb;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import com.jayway.restassured.specification.RequestSpecification;
import com.jayway.restassured.specification.ResponseSpecification;
import net.sweblog.jm.restlos.webappb.model.Member;
import net.sweblog.jm.restlos.webappb.model.Message;
import net.sweblog.jm.restlos.webappb.rest.MarshallerProvider;
import net.sweblog.jm.restlos.webappb.util.MapperBuilder;
import org.hamcrest.Matchers;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.requestSpecification;
import static com.jayway.restassured.RestAssured.responseSpecification;
import static com.jayway.restassured.mapper.ObjectMapperType.JACKSON_2;
import static java.lang.String.format;
import static net.sweblog.jm.restlos.webappb.WebAppBConstansts.PATH_TO_MSG_COLLECTION;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.core.IsEqual.equalTo;

import static com.jayway.restassured.config.ObjectMapperConfig.objectMapperConfig;
import static org.testng.Assert.fail;


/**
 *
 *
 */
@Test
@RunAsClient
public class MessageCollectionIT
    extends Arquillian
{
    @ArquillianResource
    private URL baseURL;

    @Deployment
    public static WebArchive buildDeployment()
    {
        WebArchive war = ShrinkWrap.create(WebArchive.class);

        war.addPackage(WebAppB.class.getPackage());
        war.addPackage(Message.class.getPackage());
        war.addPackage(MarshallerProvider.class.getPackage());

        war.addAsWebInfResource("WEB-INF/glassfish-web.xml")
           .addAsWebInfResource("WEB-INF/web.xml");

        return war;
    }

    @BeforeClass
    public void configureObjectMapperForRestAssured()
    {
        ObjectMapperConfig objectMapperConfig = objectMapperConfig().jackson2ObjectMapperFactory(
            new Jackson2ObjectMapperFactory() {
                @Override
                public com.fasterxml.jackson.databind.ObjectMapper create(Class c, String s) {
                    return new MapperBuilder().build();
                }
            });

        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(objectMapperConfig);
    }

    @BeforeMethod
    public void configureRestAssured()
    {
        int port = baseURL.getPort();
        String host = baseURL.getHost();
        String basePath = baseURL.getPath();
        String protocol = baseURL.getProtocol();

        RestAssured.baseURI = format("%s://%s", protocol, host);
        RestAssured.port = port;
        RestAssured.basePath = basePath;
    }


    public void canPostNewMessage() throws Exception
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .log().headers()
                   .statusCode(201)
                   .contentType(ContentType.JSON)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);
    }

    public void postANewMessageButWithUnspecifiedContentType()
    {
        long ts = System.currentTimeMillis();

        // Unter welcher URL erwarten wir die Nachricht?
        String baseLocation = baseURL.toExternalForm();

        baseLocation = baseLocation + "restlos/b/messages/" +
            String.valueOf(ts);

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .log().all()
                   .contentType(ContentType.JSON)
                   .statusCode(415)

                   .when().log().all()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.expect()
                   .log().all()
                   .statusCode(404)

                   .when()
                   .head(baseLocation);
    }

    public void postNewMessageForTheFirstTime()
    {
        long ts = System.currentTimeMillis();

        // Unter welcher URL erwarten wir die Nachricht?
        String baseLocation = baseURL.toExternalForm();

        baseLocation = baseLocation + "restlos/b/messages/" +
                       String.valueOf(ts);

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.expect()
                   .statusCode(404)

                   .when()
                   .head(PATH_TO_MSG_COLLECTION + "/{id}", ts);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(allOf(greaterThanOrEqualTo(200),lessThanOrEqualTo(202)))
        .header("Location", baseLocation)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.expect()
                   .statusCode(200)

                   .when()
                   .head(baseLocation);
    }

    public void postSameMessageTwice()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.expect()
                   .statusCode(404)

                   .when()
                   .head(PATH_TO_MSG_COLLECTION + "/{id}", ts);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(201)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(200)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);
    }

    public void putMassageForTheFirstTime()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.expect()
                   .statusCode(404)

                   .when()
                   .head(PATH_TO_MSG_COLLECTION + "/{id}", ts);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(201)

                   .when()
                   .put(PATH_TO_MSG_COLLECTION + "/{id}", ts);

        RestAssured.expect()
                   .statusCode(200)

                   .when()
                   .head(PATH_TO_MSG_COLLECTION + "/{id}", ts);
    }

    public void putMessageAndUpdateAfterwards()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(201)

                   .when()
                   .put(PATH_TO_MSG_COLLECTION + "/{id}", ts);

        Member toUpdate = new Member("Sunny Herbst", "herb");
        outMsg.setTo(toUpdate);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(200)

                   .when()
                   .put(PATH_TO_MSG_COLLECTION + "/{id}", ts);

    }

    public void deleteNonExistingMessage()
    {
        RestAssured.expect()
                   .statusCode(404).contentType(ContentType.JSON)
                   .when()
                   .delete(PATH_TO_MSG_COLLECTION + "/{id}", 123L);
    }

    public void deleteExistingMessage()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(201)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.expect()
                   .log().all()
                   .statusCode(200).contentType(ContentType.JSON)

                   .when()
                   .delete(PATH_TO_MSG_COLLECTION + "/{id}", ts);
    }

    public void getANonExistingMessage()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.expect()
                   .statusCode(404).contentType(ContentType.JSON)
                   .when()
                   .get(PATH_TO_MSG_COLLECTION + "/{id}", ts);

    }

    public void getAExistingMessage()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)
                   .expect()
                   .statusCode(201)
                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.expect()
                   .log().all()
                   .statusCode(200).contentType(ContentType.JSON)
                   .when()
                   .get(PATH_TO_MSG_COLLECTION + "/{id}", ts);
    }

    public void getAExistingMessageButRequestAUnsupportedMediaType()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)
                   .expect()
                   .statusCode(201)
                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.given()
                   .header("Accept", "application/xml")

                   .expect()
                   .contentType(ContentType.JSON)
                   .statusCode(406)

                   .when()
                   .log().all()
                   .get(PATH_TO_MSG_COLLECTION + "/{id}", ts);
    }

    public void getTheRecipientOfAnExistingMessage()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(201)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.expect()
                   .log().all()
                   .statusCode(200).contentType(ContentType.JSON)

                   // Wurde der richtige Empfaenger zurueckgegeben?
                   .body("fullName", equalTo("Richy Paschulke"))
                   .body("username", equalTo("richy"))

                   .when()
                   .get(PATH_TO_MSG_COLLECTION + "/{id}/to", ts);
    }

    public void getTheTextOfAnExistingMessage()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Essen ist fertig.");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .log().all()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .log().all()
                   .statusCode(201)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.expect()
                   .log().all()
                   .statusCode(200)
                   .contentType(ContentType.JSON)
                   .body("text", equalTo("Essen ist fertig."))

                   .when()
                   .get(PATH_TO_MSG_COLLECTION + "/{id}/text", ts);
    }

    public void getTheSenderOfAnExistingMessage()
    {
        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Hallo");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(201)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        RestAssured.expect()
                   .log().all()
                   .statusCode(200)
                   .contentType(ContentType.JSON)
                   .body("fullName", equalTo("Peter Kowalke"))
                   .body("username", equalTo("kowa"))

                   .when()
                   .get(PATH_TO_MSG_COLLECTION + "/{id}/from", ts);
    }

    public void requestAndResponseSpecificationUsage()
    {

        long ts = System.currentTimeMillis();

        Message outMsg = new Message();
        Member from = new Member("Peter Kowalke", "kowa");
        Member to = new Member("Richy Paschulke", "richy");

        outMsg.setMessageId(ts);
        outMsg.setMessage("Jetzt mit Spezifikation!");
        outMsg.setFrom(from);
        outMsg.setTo(to);

        RestAssured.given()
                   .contentType(ContentType.JSON)
                   .body(outMsg, JACKSON_2)

                   .expect()
                   .statusCode(201)

                   .when()
                   .post(PATH_TO_MSG_COLLECTION);

        ResponseSpecBuilder responseBuilder = new ResponseSpecBuilder();
        RequestSpecBuilder requestBuilder = new RequestSpecBuilder();

        requestBuilder.setContentType(ContentType.JSON)
                      .addHeader("Accept", ContentType.JSON.getAcceptHeader());

        responseBuilder.expectStatusCode(200)
                       .expectContentType(ContentType.JSON);

        ResponseSpecification specOfResponse = responseBuilder.build();
        RequestSpecification specOfRequest = requestBuilder.build();

        RestAssured.given()
                   .spec(specOfRequest)

                   .expect()
                   .spec(specOfResponse)

                   .when()
                   .get(PATH_TO_MSG_COLLECTION + "/{id}", ts);
    }
}
