package net.sweblog.jm.restlos.webappb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 *
 */
public class Member
{
    private String fullName;

    private String username;

    public Member()
    {
    }

    public Member(String fullName, String username)
    {
        this.fullName = fullName;
        this.username = username;
    }

    @JsonProperty("fullName")
    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String name)
    {
        fullName = name;
    }

    @JsonProperty("username")
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String name)
    {
        username = name;
    }
}
