package net.sweblog.jm.restlos.webappb.exceptionmappers;


import net.sweblog.jm.restlos.webappb.rest.ResponseBuilder;

import javax.inject.Singleton;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class CommonClientErrorExceptionMapper
  implements ExceptionMapper<ClientErrorException>
{
    public Response toResponse(ClientErrorException e)
    {
        Response.StatusType status = e.getResponse().getStatusInfo();
        Response r = ResponseBuilder.is(status).build();

        return r;
    }
}