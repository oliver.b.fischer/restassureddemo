package net.sweblog.jm.restlos.webappb.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 *
 *
 */
public class MetaData
{
    private DateTime received;
    private DateTime modified;

    @JsonProperty("received")
    public DateTime getReceived()
    {
        return received;
    }

    public void setReceived(DateTime pit)
    {
        received = pit;
    }

    @JsonProperty("modified")
    public DateTime getModified()
    {
        return modified;
    }

    public void setModified(DateTime pit)
    {
        modified = pit;
    }
}
