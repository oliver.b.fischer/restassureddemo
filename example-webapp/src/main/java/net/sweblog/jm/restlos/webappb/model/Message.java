package net.sweblog.jm.restlos.webappb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message
{
    private Member from;

    private Member to;

    private String message;
    private Long messageId;


    @JsonProperty("from")
    public Member getFrom()
    {
        return from;
    }

    public void setFrom(Member sender)
    {
        from = sender;
    }

    @JsonProperty("message")
    public String getMessage()
    {
        return message;
    }

    public void setMessage(String messageText)
    {
       message = messageText;
    }

    @JsonProperty("to")
    public Member getTo()
    {
        return to;
    }

    public void setTo(Member recipient)
    {
        to = recipient;
    }

    @JsonProperty("messageId")
    public Long getMessageId()
    {
        return messageId;
    }

    public void setMessageId(Long id)
    {
        messageId = id;
    }

    public Message withMessageId(Long id)
    {
        setMessageId(id);

        return this;
    }
}
