package net.sweblog.jm.restlos.webappb.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.sweblog.jm.restlos.webappb.util.MapperBuilder;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
public class MarshallerProvider
        implements MessageBodyReader<Object>, MessageBodyWriter<Object>
{

    private ObjectMapper mapper = new MapperBuilder().build();

    @Override
    public boolean isReadable(Class<?> type, Type type1,
                              Annotation[] antns,
                              MediaType mt)
    {
        return true;
    }

    @Override
    public boolean isWriteable(Class<?> type, Type type1,
                               Annotation[] antns, MediaType mt)
    {
        return true;
    }

    @Override
    public Object readFrom(Class<Object> type, Type type1, Annotation[] antns,
                           MediaType mt, MultivaluedMap<String, String> mm,
                           InputStream in)
      throws IOException, WebApplicationException
    {
        Object readObject = mapper.readValue(in, type);

        return readObject;
    }

    @Override
    public void writeTo(Object t, Class<?> type, Type type1, Annotation[] antns,
                        MediaType mt, MultivaluedMap<String, Object> mm,
                        OutputStream out)
      throws IOException, WebApplicationException
    {
        mapper.writeValue(out, t);
    }



    @Override
    public long getSize(Object t, Class<?> type, Type type1,
                        Annotation[] antns, MediaType mt)
    {
        /* Deprecated by JAX-RS 2.0 and ignored by Jersey runtime. */
        return -1L;
    }
}
