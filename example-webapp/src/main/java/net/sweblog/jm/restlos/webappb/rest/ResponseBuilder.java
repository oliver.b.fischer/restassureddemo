package net.sweblog.jm.restlos.webappb.rest;

import net.sweblog.jm.restlos.webappb.model.Member;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

/**
 *
 *
 */
public class ResponseBuilder
{

    private final Response.ResponseBuilder jaxrsBuilder;

    private ResponseBuilder(Response.StatusType statusType,
                            StatusResponse status)
    {
        jaxrsBuilder = Response.status(statusType)
                               .type(MediaType.APPLICATION_JSON_TYPE)
                               .entity(status);
    }

    public static ResponseBuilder is(Response.StatusType status)
    {
        StatusResponse response = new StatusResponse();

        response.setCode(status.getStatusCode());
        response.setStatus(status.getReasonPhrase());

        ResponseBuilder builder = new ResponseBuilder(status, response);

        return builder;
    }

    public Response build()
    {
        return jaxrsBuilder.build();
    }

    public ResponseBuilder location(URI location)
    {
        jaxrsBuilder.location(location);

        return this;
    }

    public ResponseBuilder entity(Object entity)
    {
        jaxrsBuilder.entity(entity);

        return this;
    }
}
