package net.sweblog.jm.restlos.webappb.rest;

import net.sweblog.jm.restlos.webappb.model.Message;

/**
 *
 *
 */
public class MessageContainer
{

    private Message message;

    private MetaData metaData;

    public Message getMessage()
    {
        return message;
    }

    public void setMessage(Message msg)
    {
        this.message = msg;
    }

    public MetaData getMetaData()
    {
        return metaData;
    }

    public void setMetaData(MetaData data)
    {
        this.metaData = data;
    }
}
