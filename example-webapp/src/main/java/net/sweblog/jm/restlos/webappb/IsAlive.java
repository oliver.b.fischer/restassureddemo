package net.sweblog.jm.restlos.webappb;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.*;

/**
 *
 *
 */
@Path(WebAppBConstansts.IS_ALIVE_SUB_PATH)
public class IsAlive
{
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response get()
    {
        String message = String.format("WebApplication %s is alive!",
                                       WebAppBConstansts.NAME);

        return Response.status(Status.OK)
                       .entity(message).build();
    }

}
