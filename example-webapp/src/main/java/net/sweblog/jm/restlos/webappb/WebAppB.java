package net.sweblog.jm.restlos.webappb;

import net.sweblog.jm.restlos.webappb.exceptionmappers.CommonClientErrorExceptionMapper;
import net.sweblog.jm.restlos.webappb.rest.MarshallerProvider;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath(WebAppBConstansts.APP_SUB_PATH)
public class WebAppB
    extends Application
{
    @Override
    public Set<Class<?>> getClasses()
    {
        HashSet<Class<?>> classes = new HashSet<>();

        classes.add(IsAlive.class);
        classes.add(MessageCollection.class);
        classes.add(MarshallerProvider.class);

        // Exceptions Mappers
        classes.add(CommonClientErrorExceptionMapper.class);

        return classes;
    }
}
