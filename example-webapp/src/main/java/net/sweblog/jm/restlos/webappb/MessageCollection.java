package net.sweblog.jm.restlos.webappb;

import net.sweblog.jm.restlos.webappb.model.Member;
import net.sweblog.jm.restlos.webappb.model.Message;
import net.sweblog.jm.restlos.webappb.model.MessageTextWrapper;
import net.sweblog.jm.restlos.webappb.rest.MessageContainer;
import net.sweblog.jm.restlos.webappb.rest.MetaData;
import net.sweblog.jm.restlos.webappb.rest.ResponseBuilder;
import org.joda.time.DateTime;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
@Path(WebAppBConstansts.MSG_COLLECTION_SUB_PATH)
@Produces(MediaType.APPLICATION_JSON)
public class MessageCollection
{

    Map<Long, MessageContainer> messages =
        Collections.synchronizedMap(new HashMap<Long, MessageContainer>());

    @Context
    UriInfo uriInfo;

    @Context
    Request request;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response handlePost(Message incomingMessage)
    {
        DateTime timestamp = DateTime.now();

        Long id = incomingMessage.getMessageId();

        URI location = buildLocation(uriInfo, id);

        Response response = ResponseBuilder.is(Response.Status.CREATED)
                                           .location(location)
                                           .build();

        MessageContainer container = messages.get(id);

        boolean doesMessageExist = container != null;

        if (doesMessageExist) {
            container.setMessage(incomingMessage);

            response = ResponseBuilder.is(Response.Status.OK)
                                      .location(location)
                                      .build();
        }


        MetaData meta = new MetaData();
        MessageContainer cont = new MessageContainer();

        meta.setReceived(timestamp);
        meta.setModified(timestamp);
        cont.setMessage(incomingMessage);
        cont.setMetaData(meta);

        messages.put(id, cont);

        return response;
    }

    @PUT
    @Path("{id}")
    public Response handlePut(@PathParam("id") Long id, Message incomingMessage)
    {
        DateTime timestamp = DateTime.now();

        Response response = ResponseBuilder.is(Response.Status.OK)
                                           .build();

        MessageContainer container = messages.get(id);

        boolean messageDoesNotExists = container == null;

        if (messageDoesNotExists) {
            MetaData meta = new MetaData();
            MessageContainer cont = new MessageContainer();

            meta.setReceived(timestamp);
            meta.setModified(timestamp);
            cont.setMessage(incomingMessage);
            cont.setMetaData(meta);

            messages.put(id, cont);

            response = ResponseBuilder.is(Response.Status.CREATED)
                                      .build();
        } else {
            MetaData meta = container.getMetaData();

            meta.setModified(timestamp);
            container.setMessage(incomingMessage);
        }

        return response;
    }

    @GET
    @Path("{id}")
    public Response handleGetOfMessage(@PathParam("id") Long messageId)
    {
        Response response = ResponseBuilder.is(Response.Status.NOT_FOUND)
                                           .build();

        MessageContainer container = messages.get(messageId);

        boolean doesMessageExist = container != null;

        if (doesMessageExist) {
            Message message = container.getMessage();

            response = ResponseBuilder.is(Response.Status.OK)
                                      .entity(message)
                                      .build();
        }

        return response;
    }

    @HEAD
    @Path("{id}")
    public Response handleHeadToMessage(@PathParam("id") Long messageId)
    {
        Response response = ResponseBuilder.is(Response.Status.NOT_FOUND)
                                           .build();

        MessageContainer container = messages.get(messageId);

        boolean doesMessageExist = container != null;

        if (doesMessageExist) {
            response = ResponseBuilder.is(Response.Status.OK)
                                      .build();
        }

        return response;
    }

    @DELETE
    @Path("{id}")
    public Response handleDeleteOfMessage(@PathParam("id") Long messageId)
    {
        Response response = ResponseBuilder.is(Response.Status.NOT_FOUND)
                                           .build();

        MessageContainer victim = messages.remove(messageId);

        boolean didMessageExist = victim != null;

        if (didMessageExist) {
            response = ResponseBuilder.is(Response.Status.OK)
                                      .build();
        }

        return response;
    }


    @GET
    @Path("{id}/from")
    public Response handleGetOfSender(@PathParam("id") Long messageId)
    {
        Response response = ResponseBuilder.is(Response.Status.NOT_FOUND)
                                           .build();

        MessageContainer container = messages.get(messageId);

        boolean doesMessageExist = container != null;

        if (doesMessageExist) {
            Message message = container.getMessage();
            Member from = message.getFrom();

            response = ResponseBuilder.is(Response.Status.OK)
                                      .entity(from)
                                      .build();
        }

        return response;
    }


    @GET
    @Path("{id}/to")
    public Response handleGetOfRecipient(@PathParam("id") Long messageId)
    {
        Response response = ResponseBuilder.is(Response.Status.NOT_FOUND)
                                           .build();

        MessageContainer container = messages.get(messageId);

        boolean doesMessageExist = container != null;

        if (doesMessageExist) {
            Message message = container.getMessage();
            Member to = message.getTo();

            response = ResponseBuilder.is(Response.Status.OK)
                                      .entity(to)
                                      .build();
        }

        return response;
    }

    @GET
    @Path("{id}/text")
    public Response handleGetOfMessageText(@PathParam("id") Long messageId)
    {
        Response response = ResponseBuilder.is(Response.Status.NOT_FOUND)
                                           .build();

        MessageContainer container = messages.get(messageId);

        boolean doesMessageExist = container != null;

        if (doesMessageExist) {
            Message message = container.getMessage();
            String text = message.getMessage();

            Object wrapper = new MessageTextWrapper(text);

            response = ResponseBuilder.is(Response.Status.OK)
                                      .entity(wrapper)
                                      .build();
        }

        return response;

    }

    @GET
    @Path("{id}/status")
    public Response handleGetOfStatus(@PathParam("id") Long messageId)
    {
        return ResponseBuilder.is(Response.Status.OK).build();
    }

    protected URI buildLocation(UriInfo info, Long segment)
    {
        UriBuilder pathBuilder = info.getAbsolutePathBuilder();

        UriBuilder newUri = pathBuilder.path(String.valueOf(segment));

        return newUri.build();
    }
}
