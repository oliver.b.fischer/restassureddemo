package net.sweblog.jm.restlos.webappb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageTextWrapper
{
    private String text;

    public MessageTextWrapper(String messageText)
    {
        text = messageText;
    }

    public MessageTextWrapper()
    {
    }

    @JsonProperty("text")
    public String getText()
    {
        return text;
    }

    public void setText(String messageText)
    {
        text = messageText;
    }
}
