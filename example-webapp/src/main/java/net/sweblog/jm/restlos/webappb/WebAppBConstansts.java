package net.sweblog.jm.restlos.webappb;

public class WebAppBConstansts
{
    public static final String APP_SUB_PATH = "/restlos/b/";


    public static final String MSG_COLLECTION_SUB_PATH = "messages";

    public static final String IS_ALIVE_SUB_PATH = "isalive";

    public static final String PATH_TO_IS_ALIVE =
        APP_SUB_PATH + IS_ALIVE_SUB_PATH;


    public static final String PATH_TO_MSG_COLLECTION =
        APP_SUB_PATH + MSG_COLLECTION_SUB_PATH;


    public static final Object NAME = "WebApp B";
}
